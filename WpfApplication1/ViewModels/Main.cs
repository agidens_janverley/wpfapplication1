﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace WpfApplication1.ViewModels
{
    class Main : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Main()
        {
            var i = 1;
            for (; i <= 11; i++)
            {
                var c1 = new Compartment { Id = i, State = State.InUse, Filled = 1.0 / i };
                Compartments.Add(c1);
            }

            Compartments.Add(new Compartment { Id = i++, State = State.NotInUse });
            Compartments.Add(new Compartment { Id = i++, State = State.Unavailable });

            AddCommand = new DelegateCommand(
                (_) => Compartments.Add(new Compartment { Id = 2, State = State.InUse, Filled = 0.2 }));
            RemoveCommand = new DelegateCommand(
                (_) => Compartments.RemoveAt(0),
                (_) => Compartments.Any());
        }
        public ObservableCollection<Compartment> Compartments { get; } = new ObservableCollection<Compartment>();
        public ICommand AddCommand { get; }
        public ICommand RemoveCommand { get; }
    }
}
