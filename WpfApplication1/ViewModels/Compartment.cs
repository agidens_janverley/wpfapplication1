﻿using System.ComponentModel;

namespace WpfApplication1.ViewModels
{
    public enum State
    {
        InUse,
        NotInUse,
        Unavailable
    }

    public class Compartment : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool isSelected;

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (!Equals(isSelected, value))
                {
                    isSelected = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSelected)));
                }
            }
        }


        private int id;

        public int Id
        {
            get { return id; }
            set
            {
                if (!Equals(id, value))
                {
                    id = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Id)));
                }
            }
        }

        private State state;

        public State State
        {
            get { return state; }
            set
            {
                if (!Equals(state, value))
                {
                    state = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(State)));
                }
            }
        }

        private double filled;

        public double Filled
        {
            get { return filled; }
            set
            {
                if (!Equals(filled, value))
                {
                    filled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Filled)));
                }
            }
        }
    }
}
