﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace WpfApplication1.View
{
    public class EqualityConverter<T,TResult> : MarkupExtension, IValueConverter
    {
        public EqualityConverter(T test, TResult trueValue, TResult falseValue)
        {
            Test = test;
            TrueValue = trueValue;
            FalseValue = falseValue;
        }

        public T Test { get; }
        public TResult TrueValue { get; }
        public TResult FalseValue { get; }

        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is T v) || v == null)
                return FalseValue;

            return v.Equals(Test) ? TrueValue : FalseValue;
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class EqualityConverter<T> : EqualityConverter<T, bool>
    {
        public EqualityConverter(T test) : base(test, true, false)
        {
        }
    }
}
