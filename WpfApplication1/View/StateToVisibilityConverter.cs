﻿using System.Windows;
using WpfApplication1.ViewModels;

namespace WpfApplication1.View
{
    public class StateToVisibilityConverter : EqualityConverter<State, Visibility>
    {
        public StateToVisibilityConverter()
            :base(State.InUse, Visibility.Visible, Visibility.Hidden)
        {
        }
    }
}
