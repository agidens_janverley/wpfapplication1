﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using WpfApplication1.ViewModels;

namespace WpfApplication1.View
{
    public class StateColorConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var state = (State)value;

            switch (state)
            {
                case State.InUse:
                    return Colors.Black;
                case State.NotInUse:
                    return Colors.Gray;
                case State.Unavailable:
                    return Colors.Red;
                default:
                    return Colors.Blue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
