﻿using WpfApplication1.ViewModels;

namespace WpfApplication1.View
{
    public class StateToEnabledConverter : EqualityConverter<State>
    {
        public StateToEnabledConverter()
            :base(State.InUse)
        {
        }
    }
}
