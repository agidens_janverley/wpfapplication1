﻿using System.Windows;

namespace WpfApplication1.View
{
    public class BoolToVisibilityConverter : BooleanConverter<Visibility>
    {
        public BoolToVisibilityConverter() :
            base(Visibility.Visible, Visibility.Hidden)
        { }
    }
}
