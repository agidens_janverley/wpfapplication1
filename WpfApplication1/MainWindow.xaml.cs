﻿using WpfApplication1.ViewModels;

namespace WpfApplication1
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new Main();
        }
    }
}